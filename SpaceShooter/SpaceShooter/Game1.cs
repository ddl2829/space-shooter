using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using C3.XNA;

namespace SpaceShooter
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
       
        enum gameState { Loading, StartMenu, Running, Paused, GameOver, Editor };

        #region Variables

        gameState state;

        public static Game1 instance;

        public QuadTree<Meteor> meteorTree;
        public QuadTree<Enemy> enemyTree;
        public QuadTree<Laser> laserTree;

        GraphicsDeviceManager graphics;
        public Rectangle screenBounds;

        SpriteBatch spriteBatch;
        SpriteFont scoreFont;

        public Player player;

        //To prevent holding down a button from changing state rapidly
        double stateChangeDelay = 100;
        double timeSinceStateChange = 0;

        bool flashing = false;
        double timeSinceLastFlash = 0;
        double flashInterval = 500;

        public int kills = 0;
        public int playerScore = 0;

        #region Textures

        public Texture2D playerShield;
        Texture2D playerLivesGraphic;

        Texture2D background;
        List<Texture2D> backgroundElements;

        Texture2D blank;

        public Texture2D enemyShip;
        
        public Texture2D laserRed;
        public Texture2D laserGreen;
        
        public Texture2D meteorBig;
        public Texture2D meteorSmall;

        //Explosions for laser-meteor collisions
        Texture2D explosionTexture;
        Texture2D explosionTextureGreen;

        Song backgroundMusic;

        #endregion

        List<Explosion> explosions;
        List<Notification> notifications;
        List<BackgroundElement> backgroundObjects;

        #endregion

        #region Fields

        public bool CanChangeState
        {
            get
            {
                if (timeSinceStateChange > stateChangeDelay)
                {
                    timeSinceStateChange = 0;
                    return true;
                }
                return false;
            }
        }

        public Player User
        {
            get { return player; }
        }

        public List<Explosion> Explosions
        {
            get { return Explosions; }
        }

        public QuadTree<Meteor> Meteors
        {
            get { return meteorTree; }
        }

        public List<Notification> Notifications
        {
            get { return notifications; }
        }

        public QuadTree<Enemy> Enemies
        {
            get { return enemyTree; }
        }

        public QuadTree<Laser> Lasers
        {
            get { return laserTree; }
        }

        #endregion


        public Game1()
        {
            instance = this;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            screenBounds = new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);
            explosions = new List<Explosion>();
            notifications = new List<Notification>();
            backgroundElements = new List<Texture2D>();
            backgroundObjects = new List<BackgroundElement>();
            state = gameState.Loading;
        }

        protected override void Initialize()
        {
            meteorTree = new QuadTree<Meteor>(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);
            enemyTree = new QuadTree<Enemy>(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);
            laserTree = new QuadTree<Laser>(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);
            base.Initialize();
        }

        protected override void LoadContent()
        {
            //Spritefont for scores & notifications
            scoreFont = Content.Load<SpriteFont>("score");
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //Purple background
            background = Content.Load<Texture2D>("backgroundColor");
            backgroundElements.Add(Content.Load<Texture2D>("speedLine"));
            backgroundElements.Add(Content.Load<Texture2D>("starBig"));
            backgroundElements.Add(Content.Load<Texture2D>("starSmall"));
            blank = Content.Load<Texture2D>("blank");

            enemyShip = Content.Load<Texture2D>("enemyShip");

            backgroundMusic = Content.Load<Song>("loop-transit");

            //Ship textures
            List<Texture2D> shipTextures = new List<Texture2D>();
            shipTextures.Add(Content.Load<Texture2D>("player"));
            shipTextures.Add(Content.Load<Texture2D>("playerleft"));
            shipTextures.Add(Content.Load<Texture2D>("playerright"));
            playerLivesGraphic = Content.Load<Texture2D>("life");
            playerShield = Content.Load<Texture2D>("shield");

            //Lasers
            laserRed = Content.Load<Texture2D>("laserRed");
            laserGreen = Content.Load<Texture2D>("laserGreen");

            //Meteors
            meteorBig = Content.Load<Texture2D>("meteorBig");
            meteorSmall = Content.Load<Texture2D>("meteorSmall");

            //Explosions
            explosionTexture = Content.Load<Texture2D>("laserRedShot");
            explosionTextureGreen = Content.Load<Texture2D>("laserGreenShot");

            player = new Player(shipTextures, screenBounds);
            
            PrepareLevel();

            MediaPlayer.IsRepeating = true;
            MediaPlayer.Volume = .0f;

            MediaPlayer.Play(backgroundMusic);

            state = gameState.StartMenu;
        }

        protected override void UnloadContent()
        {
        }

        private void PrepareLevel()
        {
            meteorTree.Clear();
            enemyTree.Clear();
            laserTree.Clear();

            player.Reset();

            kills = 0;
            playerScore = 0;

            notifications.Clear();
            explosions.Clear();

            //Initialize random meteors
            Random rand = new Random();
            int randomAmt = rand.Next(100, 300);
            for (int i = 0; i < randomAmt; i++)
            {
                bool bigMeteor = (rand.Next() % 2 == 0) ? true : false;
                float speed = !bigMeteor ? rand.Next(2, 8) : rand.Next(1, 4);
                Meteor newmeteor = new Meteor(bigMeteor, speed, new Vector2(rand.Next(0, screenBounds.Width), rand.Next(-10000, 0)));
                meteorTree.Add(newmeteor);
                //meteors.Add(newmeteor);
            }

            int randomEnemies = rand.Next(30, 100);
            for (int i = 0; i < randomEnemies; i++)
            {
                Enemy newEnemy = new Enemy(enemyShip, new Vector2(rand.Next(0, screenBounds.Width), rand.Next(-10000, 0)), rand.Next(2, 4) * 1000, rand.Next(1, 10) / 3 * 100);
                enemyTree.Add(newEnemy);
                //enemies.Add(newEnemy);
            }
        }

        protected override void Update(GameTime gameTime)
        {
            timeSinceStateChange += gameTime.ElapsedGameTime.Milliseconds;
            //Game level keybindings
            KeyboardState keyboardState = Keyboard.GetState();

            if (state != gameState.Paused)
            {
                UpdateBackground(gameTime);
            }

            switch(state)
            {
                case gameState.Editor:
                    {
                        EditorUpdate();
                        break;
                    }
                case gameState.Loading:
                    {
                        LoadingUpdate();
                        break;
                    }
                case gameState.Paused:
                    {
                        PausedUpdate(keyboardState);
                        break;
                    }
                case gameState.GameOver:
                    {
                        GameOverUpdate(gameTime, keyboardState);
                        break;
                    }
                case gameState.StartMenu:
                    {
                        StartMenuUpdate(gameTime, keyboardState);
                        break;
                    }
                case gameState.Running: 
                    {
                        RunningUpdate(gameTime, keyboardState);
                        break;
                    }
                default:
                    break;
            }
            base.Update(gameTime);
        }

        private void EditorUpdate()
        {
            throw new NotImplementedException();
        }

        private void LoadingUpdate()
        {
            return;
        }

        private void RunningUpdate(GameTime gameTime, KeyboardState keyboardState)
        {
            if (keyboardState.IsKeyDown(Keys.M))
                MediaPlayer.Volume = 0.0f;

            if (CanChangeState)
            {
                if (keyboardState.IsKeyDown(Keys.Escape))
                {
                    MediaPlayer.Pause();
                    state = gameState.Paused;
                    return;
                }
            }
            if (player.Lives < 0)
            {
                state = gameState.GameOver;
                return;
            }

            //Update player position, check keypresses
            player.Update(gameTime);

            List<Enemy> enemiesToRemove = new List<Enemy>();
            List<Meteor> meteorsToRemove = new List<Meteor>();
            List<Laser> lasersToRemove = new List<Laser>();
            foreach (Meteor meteor in meteorTree)
            {
                meteor.Update(gameTime);
                if (!meteor.Visible)
                    meteorsToRemove.Add(meteor);
                if(meteor.Moved)
                    meteorTree.Move(meteor);
            }

            foreach (Enemy enemy in enemyTree)
            {
                enemy.Update(gameTime);
                if (!enemy.Visible)
                    enemiesToRemove.Add(enemy);
                
                enemyTree.Move(enemy);
            }

            foreach (Laser laser in laserTree)
            {
                laser.Update(gameTime);

                //Get meteors near this laser
                List<Meteor> meteorsNearThisLaser = meteorTree.GetObjects(laser.Bounds);
                foreach (Meteor meteor in meteorsNearThisLaser)
                {
                    if (laser.Visible && laser is EnemyLaser && meteor.Visible && laser.Bounds.Intersects(meteor.Bounds))
                        laser.Visible = false;
                    if (laser.Visible && meteor.Visible && !(laser is EnemyLaser) && laser.Bounds.Intersects(meteor.Bounds))
                    {
                        laser.Visible = false;
                        meteor.Damage(laser.Damage);
                        Texture2D expTexToUse = player.LaserLevel == 0 ? explosionTexture : explosionTextureGreen;
                        explosions.Add(new Explosion(expTexToUse, laser.Position));
                    }
                }

                //Get enemies near the laser
                List<Enemy> enemiesNearThisLaser = enemyTree.GetObjects(laser.Bounds);
                foreach (Enemy enemy in enemiesNearThisLaser)
                {
                    if (laser.Visible && enemy.Visible && !(laser is EnemyLaser) && laser.Bounds.Intersects(enemy.Bounds))
                    {
                        laser.Visible = false;
                        enemy.Damage((int)laser.Damage);
                        Texture2D expTexToUse = player.LaserLevel == 0 ? explosionTexture : explosionTextureGreen;
                        explosions.Add(new Explosion(expTexToUse, laser.Position));
                    }
                }

                if (!laser.Visible)
                    lasersToRemove.Add(laser);
                
                laserTree.Move(laser);
            }

            foreach (Enemy enemy in enemiesToRemove)
                enemyTree.Remove(enemy);
            foreach (Meteor meteor in meteorsToRemove)
                meteorTree.Remove(meteor);
            foreach (Laser laser in lasersToRemove)
                laserTree.Remove(laser);
            enemiesToRemove.Clear();
            meteorsToRemove.Clear();
            lasersToRemove.Clear();

            //Get list of enemies near the player
            List<Enemy> nearbyEnemies = enemyTree.GetObjects(player.Bounds);
            foreach (Enemy enemy in nearbyEnemies)
            {
                //Enemy-Player collision
                if (enemy.Visible && enemy.Bounds.Intersects(player.Bounds))
                {
                    if (player.Shielded)
                        enemy.Damage(50);
                    else
                    {
                        player.Lives = player.Lives - 1;
                        enemy.Visible = false;
                        player.Respawn();
                    }
                }
            }
            //Get list of lasers near the player
            List<Laser> nearbyLasers = laserTree.GetObjects(player.Bounds);
            foreach (Laser laser in nearbyLasers)
            {
                //Laser-Player collision
                if (laser.Visible && laser is EnemyLaser && laser.Bounds.Intersects(player.Bounds))
                {
                    if (player.Shielded)
                        laser.Visible = false;
                    else
                    {
                        player.Lives = player.Lives - 1;
                        laser.Visible = false;
                        player.Respawn();
                    }
                }
            }
            //List of meteors near the player
            List<Meteor> nearbyMeteors = meteorTree.GetObjects(player.Bounds);
            foreach (Meteor meteor in nearbyMeteors)
            {
                //Meteor-player collision
                if (meteor.Visible && meteor.Bounds.Intersects(player.Bounds) && !player.Invincible)
                {
                    player.Lives = player.Lives - 1;
                    meteor.Visible = false;
                    player.Respawn();
                }

                if (meteor.Visible && meteor.Bounds.Intersects(player.Bounds) && player.Shielded)
                {
                    meteor.Damage(50);
                }
            }            

            //Update notifications
            for (int i = notifications.Count - 1; i > -1; i--)
            {
                notifications[i].Update(gameTime);
                if (!notifications[i].Visible)
                    notifications.RemoveAt(i);
            }            

            for (int i = explosions.Count - 1; i > -1; i--)
            {
                explosions[i].Update(gameTime);
                if (!explosions[i].Visible)
                    explosions.RemoveAt(i);
            }
        }

        private void UpdateBackground(GameTime gameTime)
        {
            //Update background elements
            if (backgroundObjects.Count < 15)
                backgroundObjects.Add(new BackgroundElement(backgroundElements, screenBounds));

            //Update background objects
            for (int i = backgroundObjects.Count - 1; i >= 0; i--)
            {
                backgroundObjects[i].Update(gameTime);
                if (backgroundObjects[i].BelowScreen)
                    backgroundObjects.RemoveAt(i);
            }
        }

        private void PausedUpdate(KeyboardState keyboardState)
        {
            if (keyboardState.IsKeyDown(Keys.Escape))
            {
                if (CanChangeState)
                {
                    MediaPlayer.Resume();
                    state = gameState.Running;
                }
                return;
            }
            if (keyboardState.IsKeyDown(Keys.Enter))
            {
                if (CanChangeState)
                {
                    state = gameState.GameOver;
                    MediaPlayer.Resume();
                }
                return;
            }
        }

        private void GameOverUpdate(GameTime gameTime, KeyboardState keyboardState)
        {
            timeSinceLastFlash += gameTime.ElapsedGameTime.Milliseconds;
            if (timeSinceLastFlash > flashInterval)
            {
                flashing = !flashing;
                timeSinceLastFlash = 0;
            }

            if (CanChangeState)
            {
                if (keyboardState.IsKeyDown(Keys.Enter))
                {
                    PrepareLevel();
                    state = gameState.Running;
                    return;
                }
                if (keyboardState.IsKeyDown(Keys.Escape))
                {
                    Exit();
                }
            }
        }

        private void StartMenuUpdate(GameTime gameTime, KeyboardState keyboardState)
        {
            timeSinceLastFlash += gameTime.ElapsedGameTime.Milliseconds;
            if (timeSinceLastFlash > flashInterval)
            {
                flashing = !flashing;
                timeSinceLastFlash = 0;
            }
            if (keyboardState.IsKeyDown(Keys.Enter))
            {
                state = gameState.Running;
                return;
            }
            if (keyboardState.IsKeyDown(Keys.Escape))
                Exit();
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();

            

            BackgroundDraw();

            switch(state)
            {
                case gameState.GameOver:
                    {
                        GameOverDraw(); break;
                    }
                case gameState.StartMenu:
                    {
                        StartMenuDraw();
                        break;
                    }
                case gameState.Paused:
                    {
                        RunningDraw();
                        PausedDraw();
                        break;
                    }
                case gameState.Running:
                    {
                        RunningDraw();
                        break;
                    }
                
                }

#if DEBUG
            DrawQuad(meteorTree, 0);
            DrawQuad(laserTree, 0);
            DrawQuad(enemyTree, 0);
#endif
            spriteBatch.End();
            base.Draw(gameTime);
        }

        private void BackgroundDraw()
        {
            spriteBatch.Draw(background, new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Color.White);

            foreach (BackgroundElement element in backgroundObjects)
                element.Draw(spriteBatch);
        }

        private void GameOverDraw()
        {
            spriteBatch.DrawString(scoreFont, "Game Over", new Vector2((int)screenBounds.Width / 2 - scoreFont.MeasureString("Game Over").X / 2, (int)screenBounds.Height / 4), Color.White);
            spriteBatch.DrawString(scoreFont, "Score: " + playerScore * 100, new Vector2((int)screenBounds.Width / 2 - scoreFont.MeasureString("Score: " + playerScore * 100).X / 2, (int)screenBounds.Height / 4 + scoreFont.MeasureString("Score: " + playerScore * 100).Y), Color.White);
            Color flashColor = flashing ? Color.White : Color.Yellow;
            spriteBatch.DrawString(scoreFont, "Press Enter to Play Again", new Vector2((int)screenBounds.Width / 2 - scoreFont.MeasureString("Press Enter to Play Again").X / 2, (int)screenBounds.Height / 3 * 2), flashColor);
            spriteBatch.DrawString(scoreFont, "Press Escape to Quit", new Vector2((int)screenBounds.Width / 2 - scoreFont.MeasureString("Press Escape to Quit").X / 2, (int)screenBounds.Height / 4 * 3), Color.White);

        }

        private void StartMenuDraw()
        {
            spriteBatch.DrawString(scoreFont, "Simple Space Shooter", new Vector2((int)screenBounds.Width / 2 - scoreFont.MeasureString("Simple Space Shooter").X / 2, (int)screenBounds.Height / 4), Color.White);
            spriteBatch.DrawString(scoreFont, "By Ddl2829", new Vector2((int)screenBounds.Width / 2 - scoreFont.MeasureString("By Ddl2829").X / 2, (int)screenBounds.Height / 4 + scoreFont.MeasureString("By Ddl2829").Y), Color.White);
            Color flashColor = flashing ? Color.White : Color.Yellow;
            spriteBatch.DrawString(scoreFont, "Press Enter to Play", new Vector2((int)screenBounds.Width / 2 - scoreFont.MeasureString("Press Enter to Play").X / 2, (int)screenBounds.Height / 3 * 2), flashColor);
            spriteBatch.DrawString(scoreFont, "Press Escape to Quit", new Vector2((int)screenBounds.Width / 2 - scoreFont.MeasureString("Press Escape to Quit").X / 2, (int)screenBounds.Height / 4 * 3), Color.White);

        }

        private void PausedDraw()
        {
            spriteBatch.DrawString(scoreFont, "Paused", new Vector2((int)screenBounds.Width / 2 - scoreFont.MeasureString("Paused").X / 2, (int)screenBounds.Height / 3), Color.White);
            spriteBatch.DrawString(scoreFont, "Press Enter to End Game", new Vector2((int)screenBounds.Width / 2 - scoreFont.MeasureString("Press Enter to End Game").X / 2, (int)screenBounds.Height / 2), Color.White);
        }

        private void RunningDraw()
        {
            player.Draw(spriteBatch);

            foreach (Meteor meteor in meteorTree)
                meteor.Draw(spriteBatch);

            foreach (Enemy enemy in enemyTree)
                enemy.Draw(spriteBatch);

            foreach (Laser laser in laserTree)
                laser.Draw(spriteBatch);

            foreach (Explosion explosion in explosions)
                explosion.Draw(spriteBatch);

            for (int i = 0; i < player.Lives; i++)
                spriteBatch.Draw(playerLivesGraphic, new Rectangle(40 * i + 10, 10, playerLivesGraphic.Width, playerLivesGraphic.Height), Color.White);

            string scoreText = "" + playerScore * 100;
            spriteBatch.DrawString(scoreFont, scoreText, new Vector2(screenBounds.Width - scoreFont.MeasureString(scoreText).X - 30, 5), Color.White);

            spriteBatch.Draw(blank, new Rectangle(8, 43, (int)player.MaxShieldPower / 30 + 4, 24), Color.Black);
            spriteBatch.Draw(blank, new Rectangle(10, 45, (int)player.MaxShieldPower / 30, 20), Color.White);
            spriteBatch.Draw(blank, new Rectangle(10, 45, (int)player.ShieldPower / 30, 20), Color.Blue);

            if (player.ShieldCooldown)
                spriteBatch.Draw(blank, new Rectangle(10, 45, (int)player.ShieldPower / 30, 20), Color.Red);

            foreach (Notification notification in notifications)
                notification.Draw(spriteBatch, scoreFont);
        }

        private void DrawQuad(QuadTree<Meteor> quad, int depth)
        {
            if (quad != null)
            {
                DrawQuad(quad.RootQuad, depth);
            }
        }

        private void DrawQuad(QuadTree<Enemy> quad, int depth)
        {
            if (quad != null)
            {
                DrawQuad(quad.RootQuad, depth);
            }
        }

        private void DrawQuad(QuadTree<Laser> quad, int depth)
        {
            if (quad != null)
            {
                DrawQuad(quad.RootQuad, depth);
            }
        }

        private void DrawQuad(QuadTreeNode<Meteor> quad, int depth)
        {
            if (quad != null)
            {
                Rectangle rect = quad.QuadRect;

                Point mid = new Point(
                    rect.X + rect.Width / 2,
                    rect.Y + rect.Height / 2
                    );

                Color drawColor = Color.White;
                switch (depth)
                {
                    case 0:
                        drawColor = Color.White;
                        break;
                    case 1:
                        drawColor = Color.Red;
                        break;
                    case 2:
                        drawColor = Color.Green;
                        break;
                    case 3:
                        drawColor = Color.Blue;
                        break;
                    case 4:
                        drawColor = Color.Gray;
                        break;
                    case 5:
                        drawColor = Color.DarkRed;
                        break;
                    case 6:
                        drawColor = Color.DarkGreen;
                        break;
                    case 7:
                        drawColor = Color.DarkBlue;
                        break;
                    default:
                        drawColor = Color.White;
                        break;
                }

                //Primative.Box(rect.Left, rect.Top, rect.Right, rect.Bottom, 1, drawColor);
                Primitives2D.DrawRectangle(spriteBatch, rect, drawColor, 1);

                DrawQuad(quad.TopLeftChild, depth + 1);
                DrawQuad(quad.TopRightChild, depth + 1);
                DrawQuad(quad.BottomLeftChild, depth + 1);
                DrawQuad(quad.BottomRightChild, depth + 1);
            }
        }

        private void DrawQuad(QuadTreeNode<Enemy> quad, int depth)
        {
            if (quad != null)
            {
                Rectangle rect = quad.QuadRect;

                Point mid = new Point(
                    rect.X + rect.Width / 2,
                    rect.Y + rect.Height / 2
                    );

                Color drawColor = Color.White;
                switch (depth)
                {
                    case 0:
                        drawColor = Color.White;
                        break;
                    case 1:
                        drawColor = Color.Red;
                        break;
                    case 2:
                        drawColor = Color.Green;
                        break;
                    case 3:
                        drawColor = Color.Blue;
                        break;
                    case 4:
                        drawColor = Color.Gray;
                        break;
                    case 5:
                        drawColor = Color.DarkRed;
                        break;
                    case 6:
                        drawColor = Color.DarkGreen;
                        break;
                    case 7:
                        drawColor = Color.DarkBlue;
                        break;
                    default:
                        drawColor = Color.White;
                        break;
                }

                //Primative.Box(rect.Left, rect.Top, rect.Right, rect.Bottom, 1, drawColor);
                Primitives2D.DrawRectangle(spriteBatch, rect, drawColor, 1);

                DrawQuad(quad.TopLeftChild, depth + 1);
                DrawQuad(quad.TopRightChild, depth + 1);
                DrawQuad(quad.BottomLeftChild, depth + 1);
                DrawQuad(quad.BottomRightChild, depth + 1);
            }
        }

        private void DrawQuad(QuadTreeNode<Laser> quad, int depth)
        {
            if (quad != null)
            {
                Rectangle rect = quad.QuadRect;

                Point mid = new Point(
                    rect.X + rect.Width / 2,
                    rect.Y + rect.Height / 2
                    );

                Color drawColor = Color.White;
                switch (depth)
                {
                    case 0:
                        drawColor = Color.White;
                        break;
                    case 1:
                        drawColor = Color.Red;
                        break;
                    case 2:
                        drawColor = Color.Green;
                        break;
                    case 3:
                        drawColor = Color.Blue;
                        break;
                    case 4:
                        drawColor = Color.Gray;
                        break;
                    case 5:
                        drawColor = Color.DarkRed;
                        break;
                    case 6:
                        drawColor = Color.DarkGreen;
                        break;
                    case 7:
                        drawColor = Color.DarkBlue;
                        break;
                    default:
                        drawColor = Color.White;
                        break;
                }

                //Primative.Box(rect.Left, rect.Top, rect.Right, rect.Bottom, 1, drawColor);
                Primitives2D.DrawRectangle(spriteBatch, rect, drawColor, 1);

                DrawQuad(quad.TopLeftChild, depth + 1);
                DrawQuad(quad.TopRightChild, depth + 1);
                DrawQuad(quad.BottomLeftChild, depth + 1);
                DrawQuad(quad.BottomRightChild, depth + 1);
            }
        }
    }
}
