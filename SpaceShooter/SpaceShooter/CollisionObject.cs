﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpaceShooter
{
    public abstract class CollisionObject : QuadStorable
    {
        public bool HasMoved { get; set; }
        public Rectangle Rect { get; set; }
        public abstract bool Visible { get; set; }
        public abstract void Update(GameTime gameTime);
    }
}
